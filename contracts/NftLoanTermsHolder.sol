pragma solidity ^0.8.0;
/*Developed By Monzer Masri */
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import {LoanOfferLib} from "./LoanOfferLib.sol"; 

contract NftLoanTermsHolder {
  LoanOfferLib.LoanOffer acceptedLoanOffer;
  address borrower;
  uint loanPeriodEndTime;
  address contractOwner;

  event LoanPaidBack(
    address borrower,
    address lender,
    address nftAddress, 
    uint nftId
  );
  
  event CollateralLiquidated(
    address borrower,
    address lender,
    address nftAddress, 
    uint nftId
  );

  constructor(LoanOfferLib.LoanOffer memory _acceptedLoanOffer, address _borrower) public {
    acceptedLoanOffer = _acceptedLoanOffer;
    borrower = _borrower;
    loanPeriodEndTime = block.timestamp + _acceptedLoanOffer.loanPeriod * 86400;
    contractOwner = msg.sender;
  }

  
  function payBackLoan() external{
      require(block.timestamp <= loanPeriodEndTime, "Too late to payback!");
      require(msg.sender == borrower, "Only Borrower can payback!");
      
      //forward money to lender.
      ERC20(acceptedLoanOffer.loanToken).transferFrom(borrower, acceptedLoanOffer.lender, acceptedLoanOffer.returnAmount);
   
      //return nft to borrower;
      ERC721(acceptedLoanOffer.nftAddress).transferFrom(address(this), borrower, acceptedLoanOffer.nftId);

      emit LoanPaidBack(borrower, acceptedLoanOffer.lender, acceptedLoanOffer.nftAddress, acceptedLoanOffer.nftId);
  }

  function liquidateCollateral() external {
    require(msg.sender == acceptedLoanOffer.lender, "Only lender is allowed to Liquidate!");
    require(block.timestamp > loanPeriodEndTime, "Too early to Liquidate!");

    //forward NFT to lender. 
    ERC721(acceptedLoanOffer.nftAddress).transferFrom(address(this), acceptedLoanOffer.lender, acceptedLoanOffer.nftId);
    emit CollateralLiquidated(borrower, acceptedLoanOffer.lender, acceptedLoanOffer.nftAddress, acceptedLoanOffer.nftId);
  }

  function getBorrower() public view returns(address){
    return borrower;
  }
  
  function getLender() public view returns(address){
    return acceptedLoanOffer.lender;
  }

  function getLoanOfferTerms() public view returns(LoanOfferLib.LoanOffer memory){
    return acceptedLoanOffer;
  }

  function isLoanPeriodEnded() public view returns (bool){
    return (block.timestamp > loanPeriodEndTime);
  }

}

 
