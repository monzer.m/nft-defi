pragma solidity ^0.8.0;
/*Developed By Monzer Masri */

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

import "@openzeppelin/contracts/utils/cryptography/draft-EIP712.sol";
import "@openzeppelin/contracts/utils/cryptography/SignatureChecker.sol";

import "@openzeppelin/contracts/utils/Strings.sol";
import "./NftLoanTermsHolder.sol"; 
import {LoanOfferLib} from "./LoanOfferLib.sol"; 

contract NftDefiProtocol is EIP712('NftDefiProtocol', '1'){

 mapping(address => address[]) lenders;
 mapping(address => address[]) borrowers;
 mapping(address =>  mapping(uint => address)) loanTermsHolderContracts;
 mapping(bytes32 => bool) cancelledOffers;

  bytes32 constant public LoanOffer_TYPEHASH = keccak256(
        "LoanOffer(address nftAddress,uint nftId,address loanToken,uint loanAmount,uint returnAmount,uint loanPeriod,address lender,uint expireTime)"
    );

  event OfferAccepted(
      bytes32 offerHash, 
      address borrower,
      address lender,
      address termsHolderContract
  );

  constructor() public {
  }
 

  function acceptLoanOffer(LoanOfferLib.LoanOffer memory loanOffer, bytes calldata signature) external {
    //require offer not expired and not cancelled. 
    require(block.timestamp < loanOffer.expireTime, "This offer is expired!");

    bytes32 offerHash = calcLoanOfferHash(loanOffer);
    require(SignatureChecker.isValidSignatureNow(loanOffer.lender, offerHash, signature), "LOP: bad signature");
    require(!cancelledOffers[offerHash], "Offer is no longer available.");

    NftLoanTermsHolder nftLoanTermsHolder = new NftLoanTermsHolder(loanOffer, msg.sender);

    ERC721(loanOffer.nftAddress).transferFrom(address(msg.sender),  address(this) , loanOffer.nftId);
    ERC721(loanOffer.nftAddress).transferFrom(address(this), address(nftLoanTermsHolder), loanOffer.nftId);
    ERC20(loanOffer.loanToken).transferFrom(loanOffer.lender,  address(msg.sender),  loanOffer.loanAmount);
    
    lenders[loanOffer.lender].push(address(nftLoanTermsHolder));
    borrowers[msg.sender].push(address(nftLoanTermsHolder));
    loanTermsHolderContracts[loanOffer.nftAddress][loanOffer.nftId] = address(nftLoanTermsHolder);

    emit OfferAccepted(offerHash, msg.sender, loanOffer.lender, address(nftLoanTermsHolder));
  }

  function cancelLoanOffer(LoanOfferLib.LoanOffer memory loanOffer, bytes calldata signature) external {
    //cancel 
    bytes32 offerHash = calcLoanOfferHash(loanOffer);
    require(SignatureChecker.isValidSignatureNow(loanOffer.lender, offerHash, signature), "LOP: bad signature");

    cancelledOffers[offerHash] = true;
  }
 
 function calcLoanOfferHash(LoanOfferLib.LoanOffer memory loanOffer) public view returns(bytes32){
    return _hashTypedDataV4(keccak256(abi.encode(LoanOffer_TYPEHASH, loanOffer)));
 }

function getLoanTermsHolderContract(address nft, uint nftId) public view returns(address){
    return loanTermsHolderContracts[nft][nftId];
}

function getLendedNFTs(address lender) public view returns(address[] memory){
    return lenders[lender];
} 

function getLendedNFTs(address lender, address nftAddress, uint nftId) public view returns(address[] memory){
    return lenders[lender];
} 

function stringToBytes32(string memory source) public pure returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
   
}