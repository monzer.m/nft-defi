pragma solidity ^0.8.0;


library LoanOfferLib {

  struct LoanOffer{
      address nftAddress; 
      uint   nftId;
      address loanToken;
      uint loanAmount ;
      uint  returnAmount ;
      uint   loanPeriod;
      address  lender; 
      uint expireTime;
  }
}