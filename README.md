# NftDefiProtocol
NftDefiProtocol is a decentralized protocol allows Borrowers to use their NFTs as a collateral to access the liquidity they need. Lenders are incentivized by either the APR profit they get back from the borrower when the lending period ends or by liquidating the NFT thats value is more than the lended amount.




# 1- Listing your NFT (by borrower) :
Borrower calls this api to add thier NFT to the listing. He has to provide his wallet address, nft address, nft Id, prefered loan token and prefered loan amount. Borrower has also to cryptography sign the message locally with his private key and add the signature to the body of the request.

HTTP /add-nft-listing
Method : POST 
Body :
```        
{
    "nftOwner" : "0xac48aF79df268c1e59E2C42CBe19497D510FD205",
    "nftAddress" : "0xac48aF79df268c1e59E2C42CBe19497D510FD205",
    "nftId" : 2,
    "preferedLoanToken" : "0xac48aF79df268c1e59E2C42CBe19497D510FD205",
    "preferedLoanAmount" : "1000000" ,
    "signature" : "0x80065927ad953e9d889404f243ec7e48aa3d4917727f5238273a812b7b96599a31ded8ebbb2462ac73201a8b6847fe2a30b87578c7e1d15f3eb264f5f63dd3111b"

}
``` 
![](./1.PNG)

# 2- Making a loan offer (by Lender):
Lender can view listed NFTS. prepare a loan offer containing nft information, loan token, loan amount, returned amount, offer expiration time and loan period. Lender has also to cryptography sign the message locally with his private key and add the signature to the body of the request.

HTTP /add-nft-listing
Method : POST 
Body :
```        
{ 
    "nftAddress" : "0xac48aF79df268c1e59E2C42CBe19497D510FD205",
    "nftId" : 2,
    "loanToken" : "0xac48aF79df268c1e59E2C42CBe19497D510FD205",
    "loanAmount" : "1000000" ,
    "returnAmount" : "1200000" ,
    "loanPeriod" : "20" ,
    "lender" : "0xac48aF79df268c1e59E2C42CBe19497D510FD205" ,
    "expireTime" : "235126543" ,
    "signature" : "0x80065927ad953e9d889404f243ec7e48aa3d4917727f5238273a812b7b96599a31ded8ebbb2462ac73201a8b6847fe2a30b87578c7e1d15f3eb264f5f63dd3111b"
}
```

![](./2.PNG)




# 3- Accepting loan offer (by borrower):
 ```
 //NftDefiProtocol smartcontract
 function acceptLoanOffer(LoanOfferLib.LoanOffer memory loanOffer, bytes calldata signature) external 
 ```

Borrower can view offers and accpet optionally one of them. once accepted in one atomic transaction his NFT will be transfered to the ownership of the protocol, the loan amount will be transfered from the lender to borrower address, and a new smart contract holding the terms of the loan will be created on-chain to garantee the terms.


![](./3.PNG)


# 4- Pay loan amount back (by borrower):
```
//NftLoanTermsHolder smartcontract
function payBackLoan() external
```
Before the loan period ends , the borrower has to pay back the returnAmount = loan amount + APR. Once borrower pays an in one trannsaction token amount will be transfered from borrower to lender address. NFT ownership will be transfered from protocol to borrower.


![](./4.PNG)


# 5- Liquidating collateral (by lended) :
```
//NftLoanTermsHolder smartcontract
function liquidateCollateral() external 
```
Once period of loan is endede and borrower has not paid back yet. Lender has the right to liquidate the NFT and transfer it to his ownership.




# Run on Avax subnet : 
open truffle-config.js file and add avax network info to networks section to make the depolyment. 



