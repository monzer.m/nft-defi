
const NftLoanTermsHolder = artifacts.require("NftLoanTermsHolder.sol");
const NftDefiProtocol = artifacts.require("NftDefiProtocol.sol");
const BUSD = artifacts.require("BUSD.sol");
const NFT = artifacts.require("NFT.sol");
const { assert } = require('console');
const ethSigUtil = require('eth-sig-util');
const Wallet = require('ethereumjs-wallet').default;
const ethers = require('ethers');

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();
});


contract('NftDefiProtocol', async accounts => {
  const [admin, admin2, admin3, zzz, anon, _] = accounts;
  const borrower = "0xaf48fF79df268c1e59E2C42CBe19497D510FC205";
  const borrowerPK = "3d5af7f0d20a4a3687ab60de7790806fbca1cd0672ff16a89b92ab7821f023f2";
  const lender = "0x32b71be6fc0adc37bd668cd3afcd3688a979e25e" ;
  const lenderPk = 'fcf25cab6c4aacb120567b6e0c649a570d48d6ee54f350017f935f44bafd1ccb';
  var chainId = -1;
  
  before(async () => {
    chainId = await web3.eth.getChainId();
    console.log(`chainId >> ${chainId}`);
  });


  it('test1 ', async () => {
    const busd = await BUSD.new();
    const nft = await NFT.new('NFTGame', 'NGO');
    const nftId = 1;

    nft.mint(borrower, nftId);
    busd.mint(lender, web3.utils.toWei('1000'));

    const myNftDefiProtocol = await NftDefiProtocol.new();
    console.log(`NftDefiProtocol address = ${myNftDefiProtocol.address}`);
    await nft.approve(myNftDefiProtocol.address, nftId, { from: borrower });
    await busd.approve(myNftDefiProtocol.address, web3.utils.toWei('1000'), { from: lender });

    await assertBalance(busd, lender, '1000.0', 'busd-balanceOf-lender-before'); 
    await assertBalance(busd, borrower, '0.0', 'busd-balanceOf-borrower-before');

    await acceptLoanOffer(chainId, myNftDefiProtocol, nft, busd, lender);

    await assertBalance(busd, lender, '900.0', 'busd-balanceOf-lender-after-acceptance'); 
    await assertBalance(busd, borrower, '100.0', 'busd-balanceOf-lender-after-acceptance'); 

    //assert nft is gone. 
    var nftOwnerAfterAcceptance = await nft.ownerOf(nftId);
    
    //get termsContract 
    var termsHolderAddress = await myNftDefiProtocol.getLoanTermsHolderContract(nft.address, nftId);

    //assert new nft owner.
    assert(nftOwnerAfterAcceptance == termsHolderAddress);
    console.log(`termsHolder address = ${termsHolderAddress}`);

    //allow to payback 
    await busd.approve(termsHolderAddress, web3.utils.toWei('1000'), { from: borrower });
    
    //payback 
    busd.mint(borrower, web3.utils.toWei('100'));
    await callPayback(termsHolderAddress);

    //assert money is forwarded.
    await assertBalance(busd, lender, '1020.0', 'busd-balanceOf-lender-after-payBackLoan'); 
    await assertBalance(busd, borrower, '80.0', 'busd-balanceOf-lender-after-payBackLoan'); 

    //assert nft is back 
    var nftOwnerAfterpayBackLoan = await nft.ownerOf(nftId);
    assert(nftOwnerAfterpayBackLoan == borrower);

  });

  async function callPayback(termsHolderAddress){
    const termsHolderAbi = [{"inputs":[{"components":[{"internalType":"address","name":"nftAddress","type":"address"},{"internalType":"uint256","name":"nftId","type":"uint256"},{"internalType":"address","name":"loanToken","type":"address"},{"internalType":"uint256","name":"loanAmount","type":"uint256"},{"internalType":"uint256","name":"returnAmount","type":"uint256"},{"internalType":"uint256","name":"loanPeriod","type":"uint256"},{"internalType":"address","name":"lender","type":"address"},{"internalType":"uint256","name":"expireTime","type":"uint256"}],"internalType":"struct LoanOfferLib.LoanOffer","name":"_acceptedLoanOffer","type":"tuple"},{"internalType":"address","name":"_borrower","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"borrower","type":"address"},{"indexed":false,"internalType":"address","name":"lender","type":"address"},{"indexed":false,"internalType":"address","name":"nftAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"nftId","type":"uint256"}],"name":"CollateralLiquidated","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"borrower","type":"address"},{"indexed":false,"internalType":"address","name":"lender","type":"address"},{"indexed":false,"internalType":"address","name":"nftAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"nftId","type":"uint256"}],"name":"LoanPaidBack","type":"event"},{"inputs":[],"name":"payBackLoan","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"liquidateCollateral","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"getBorrower","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getLender","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getLoanOfferTerms","outputs":[{"components":[{"internalType":"address","name":"nftAddress","type":"address"},{"internalType":"uint256","name":"nftId","type":"uint256"},{"internalType":"address","name":"loanToken","type":"address"},{"internalType":"uint256","name":"loanAmount","type":"uint256"},{"internalType":"uint256","name":"returnAmount","type":"uint256"},{"internalType":"uint256","name":"loanPeriod","type":"uint256"},{"internalType":"address","name":"lender","type":"address"},{"internalType":"uint256","name":"expireTime","type":"uint256"}],"internalType":"struct LoanOfferLib.LoanOffer","name":"","type":"tuple"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"isLoanPeriodEnded","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"}];
    var customHttpProvider = new ethers.providers.JsonRpcProvider('http://127.0.0.1:9545');
    const wallet = new ethers.Wallet(borrowerPK);
    const account = wallet.connect(customHttpProvider);
    const termsHolderContract = new ethers.Contract(termsHolderAddress,  termsHolderAbi,account);
    var brr = await termsHolderContract.getBorrower();
    console.log(`brr >>  ${brr}`);

    var pbtx = await termsHolderContract.payBackLoan();
    console.log(`payBackLoan tx >>  ${JSON.stringify(pbtx)}`);
  }

  async function assertBalance(tokenContract, address, val, printMsg){
    var balcnceWei = await tokenContract.balanceOf(address);
    var balcnceEth = `${ethers.utils.formatEther(`${balcnceWei}`)}`;
    console.log(`${printMsg} = ${balcnceEth}`);
    assert(balcnceEth == val);
  }

  async function acceptLoanOffer(chainId, myNftDefiProtocol, nft, busd, lender) {
    const loanOffer = {
      nftAddress: nft.address,
      nftId: 1,
      loanToken: busd.address,
      loanAmount: web3.utils.toWei('100'),
      returnAmount: web3.utils.toWei('120'),
      loanPeriod: 30,
      lender: lender,
      expireTime: "1688600912"
    };

    const lenderWallet = Wallet.fromPrivateKey(Buffer.from(lenderPk, 'hex'));
    const signature = signLoanOffer(loanOffer, chainId, myNftDefiProtocol.address, lenderWallet.getPrivateKey());
    console.log(`Lender Signature >>> ${signature}`);

    var tx = await myNftDefiProtocol.acceptLoanOffer(loanOffer, signature, { from: borrower });
    console.log(`acceptLoanOffer tx >>  ${JSON.stringify(tx)}`);
  }

  function signLoanOffer(loanOffer, chainId, target, privateKey) {
    const data = buildLoanOfferData(chainId, target, loanOffer);
    return ethSigUtil.signTypedMessage(privateKey, { data });
  }

  const version = '1';
  const name = 'NftDefiProtocol';
  
  function buildLoanOfferData(chainId, verifyingContract, loanOffer) {
    const EIP712Domain = [
      { name: 'name', type: 'string' },
      { name: 'version', type: 'string' },
      { name: 'chainId', type: 'uint256' },
      { name: 'verifyingContract', type: 'address' },
    ];

    return {
      primaryType: 'LoanOffer',
      types: { EIP712Domain, LoanOffer },
      domain: { name, version, chainId, verifyingContract },
      message: loanOffer,
    };
  }


  const LoanOffer = [
    { name: 'nftAddress', type: 'address' },
    { name: 'nftId', type: 'uint' },
    { name: 'loanToken', type: 'address' },
    { name: 'loanAmount', type: 'uint' },
    { name: 'returnAmount', type: 'uint' },
    { name: 'loanPeriod', type: 'uint' },
    { name: 'lender', type: 'address' },
    { name: 'expireTime', type: 'uint' }
  ];


})